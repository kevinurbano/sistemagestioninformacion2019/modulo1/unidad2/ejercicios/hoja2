﻿USE ciclistas;
/* Modulo 1 Unidad 2 */

/* Consulta de seleccion 2 */

/* Consulta 1 */
SELECT COUNT(*) FROM ciclista c;

/* Consulta 2 */
SELECT COUNT(*) FROM ciclista c WHERE c.nomequipo='Banesto';

/* Consutla 3 */
SELECT AVG(c.edad) FROM ciclista c;

/* Consulta 4 */
SELECT AVG(c.edad) FROM ciclista c WHERE c.nomequipo='Banesto';

/* Consulta 5 */
SELECT c.nomequipo,AVG(c.edad) FROM ciclista c GROUP BY c.nomequipo;

/* Consulta 6 */
SELECT c.nomequipo,COUNT(*) FROM ciclista c GROUP BY c.nomequipo;

/* Consulta 7 */
SELECT COUNT(*) FROM puerto p;

/* Consulta 8 */
SELECT COUNT(*) FROM puerto p WHERE p.altura>1500;

/* Consulta 9 */
SELECT c.nomequipo FROM ciclista c GROUP BY c.nomequipo HAVING COUNT(*)>4;

/* Consulta 10 */
SELECT c.nomequipo FROM ciclista c WHERE c.edad BETWEEN 28 AND 32 GROUP BY c.nomequipo HAVING COUNT(*)>4;

/* Consulta 11 */
SELECT e.dorsal,COUNT(*) etapasGanadas FROM etapa e GROUP BY e.dorsal;

/* Consulta 12 */
SELECT e.dorsal FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>1;
